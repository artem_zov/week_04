package ru.edu.model;

import java.util.Objects;

public class AthleteImpl implements Athlete {
    /**
     * Имя, фамилия и страна спортсмена.
     *
     */

    private String firstName;
    private String lastName;
    private String country;

    /**
     * Через Builder собираем информацию по спортсменам.
     *
     * @return значение
     */

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {
        private final AthleteImpl param = new AthleteImpl();

        public Builder setFirstName(String firstName) {
            param.firstName = firstName;
            return this;
        }

        public Builder setLastName(String lastName) {
            param.lastName = lastName;
            return this;
        }

        public Builder setCountry(String country) {
            param.country = country;
            return this;
        }

        public AthleteImpl build() {
            if (param.firstName == null && param.lastName == null && param.country == null) {
                throw new IllegalArgumentException("Необходимо указать имя" +
                        "и фамилию спортсмена, также страну");
            }
            return param;
        }
    }

    /**
     * Имя.
     *
     * @return значение
     */

    @Override
    public String getFirstName() {

        return firstName;
    }

    /**
     * Фамилия.
     *
     * @return значение
     */

    @Override
    public String getLastName() {
        return lastName;
    }

    /**
     * Страна.
     *
     * @return значение
     */
    @Override
    public String getCountry() {
        return country;
    }

    @Override
    public boolean equals() {
        return false;
    }

    @Override
    public boolean equals(Object object) {

        if (object == this) {
            return true;
        }

        if (object == null) {
            return false;
        }

        if (!(this.getClass().equals(object.getClass()))) {
            return false;
        }

        AthleteImpl other = (AthleteImpl) object;
        return firstName.equals(other.firstName) &&
                lastName.equals(other.lastName) &&
                country.equals(other.country);
    }

    /**
     * hash code.
     * @return hash code
     */

    @Override
    public int hashCode() {
        return Objects.hash(firstName, lastName, country);
    }

    @Override
    public String toString() {
        return "AthleteImpl{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", country='" + country + '\'' +
                '}';
    }
}
