package ru.edu;

import ru.edu.model.Athlete;
import ru.edu.model.CountryParticipant;
import ru.edu.model.Participant;

import java.util.*;


public class CompetitionImpl implements Competition {

    /**
     * Счётчик id.
     *
     */
    private long id = 0;

    /**
     * Список участников.
     *
     *
     */
    private Map<Long, ParticipantImpl> participantsMap = new HashMap<>();

    /**
     * Все зарегистрированные спортсмены.
     *
     */
    private Set<Athlete> athletesMap = new HashSet<>();

    /**
     * Регистрация участника.
     *
     * @param participant - участник
     * @return зарегистрированный участник
     * @throws IllegalArgumentException - при попытке повторной регистрации
     */

    @Override

    public Participant register(Athlete participant) throws IllegalArgumentException{
        if (athletesMap.contains(participant)) {
            throw new IllegalArgumentException(
                    "Спортсмен уже есть в базе");
        }

        ParticipantImpl participantAddedToMap = new ParticipantImpl(++id, participant);
        athletesMap.add(participant);
        participantsMap.put(id, participantAddedToMap);
        return participantAddedToMap.getClone();
    }


    /**
     * Обновление счета участника по его id.
     * Требуется константное время выполнения.
     * <p>
     * updateScore(10) прибавляет 10 очков.
     * updateScore(-5) отнимает 5 очков.
     *
     * @param id    регистрационный номер участника
     * @param score +/- величина изменения счета
     */

    @Override
    public void updateScore(long id, long score) {
        ParticipantImpl participant = participantsMap.get(id);
        if (participant != null) {
            participant.increaseScore(score);
        }
    }


    /**
     * Обновление счета участника по его объекту Participant.
     * Требуется константное время выполнения.
     *
     * @param participant зарегистрированный участник
     * @param score       новое значение счета
     */

    @Override
    public void updateScore(Participant participant, long score) {
        if (participant != null) {
            updateScore(participant.getId(), score);
        }
    }

    /**
     * Получение результатов.
     * Сортировка участников от большего счета к меньшему.
     *
     * @return отсортированный список участников
     */

    @Override
    public List<Participant> getResults() {
        List<Participant> participantList = new LinkedList<>(participantsMap.values());
        participantList.sort(Comparator.comparingLong(Participant::getScore).reversed());
        return participantList;
    }


    /**
     * Получение результатов по странам.
     * Группировка участников из одной страны и сумма их счетов.
     * Сортировка результатов от большего счета к меньшему.
     *
     * @return отсортированный список стран-участников
     */

    @Override
    public List<CountryParticipant> getParticipantsCountries() {
        Map<String, CountryParticipant> countryParticipantMap = new HashMap<>();
        for (ParticipantImpl participant : participantsMap.values()) {
            String country = participant.getAthlete().getCountry();
            CountryParticipantImpl countryParticipant;
            if (countryParticipantMap.containsKey(country)) {
                countryParticipant = (CountryParticipantImpl)countryParticipantMap.get(country);
            } else {
                countryParticipant = new CountryParticipantImpl(country);
                countryParticipantMap.put(country, countryParticipant);
            }
            countryParticipant.addParticipant(participant.getClone());
        }

        List<CountryParticipant> countryParticipantList =
                new LinkedList<>(countryParticipantMap.values());
        countryParticipantList.sort(Comparator
                .comparingLong(CountryParticipant::getScore).reversed());
        return countryParticipantList;
    }


    private static class ParticipantImpl implements Participant {
        private final Long id;
        private final Athlete athlete;
        private long score;

        public ParticipantImpl(long id, Athlete athlete) {
            this.id = id;
            this.athlete = athlete;
            this.score = 0;
        }


        private ParticipantImpl(long id, Athlete athlete, long score) {
            this.id = id;
            this.athlete = athlete;
            this.score = score;
        }


        /**
         * Получение информации о регистрационном номере.
         *
         * @return регистрационный номер
         */

        @Override
        public Long getId() {
            return id;
        }


        /**
         * Информация о спортсмене.
         *
         * @return объект спортсмена
         */

        @Override
        public Athlete getAthlete() {
            return athlete;
        }

        /**
         * Счет участника.
         *
         * @return счет
         */

        @Override
        public long getScore() {
            return score;
        }


        /**
         * Увеличение очков участников.
         * увеличить на 10 очков.
         * отнять 5 очков.
         */

        public void increaseScore(final long newScore) {
            this.score += newScore;
        }


        @Override
        public String toString() {
            return "ParticipantImpl{" +
                    "id=" + id +
                    ", athlete=" + athlete +
                    ", score=" + score +
                    '}';
        }


        public Participant getClone() {
            return new ParticipantImpl(this.id, this.athlete, this.score);
        }
    }

    private static class CountryParticipantImpl implements CountryParticipant {

        private String name;
        private List<Participant> participants;
        private long score;

        public CountryParticipantImpl(String name) {

            this.name = name;
            this.participants = new ArrayList<>();
            this.score = 0;
        }


        /**
         * Название страны.
         *
         * @return название
         */

        @Override
        public String getName() {
            return name;
        }

        /**
         * Список участников от страны.
         *
         * @return список участников
         */

        @Override
        public List<Participant> getParticipants() {
            return participants;
        }


        /**
         * Счет страны.
         *
         * @return счет
         */

        @Override
        public long getScore() {
            return score;
        }


        /**
         * Добавление нового участника и обновление очков у страны.
         *
         */

        public void addParticipant(Participant participant) {
            participants.add(participant);
            score += participant.getScore();
        }


        @Override
        public String toString() {
            return "CountryParticipantImpl{" +
                    "name='" + name + '\'' +
                    ", score=" + score +
                    ", participants=" + participants +
                    '}';
        }
    }
}

