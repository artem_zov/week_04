import org.junit.Test;

import ru.edu.Competition;
import ru.edu.CompetitionImpl;
import ru.edu.model.Athlete;

import ru.edu.model.AthleteImpl;

import ru.edu.model.CountryParticipant;

import ru.edu.model.Participant;


import java.util.List;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class CompetitionImplTest {


    Competition competition = new CompetitionImpl();

    final static String[][] FINAL_ATHLETE = {
            {"Игорь", "Шелкунов", "Казахстан"}
            , {"Николай", "Климов", "Россия"}
            , {"Вирджиния", "Смит", "Новая Зеландия"}
            , {"Али", "Абу-Джафар", "ОАЭ"}
            , {"Кеннет", "О'Коннор", "США"}
    };


    final static long[] FINAL_GOALS = {
            500L
            , 300L
            , 250L
            , 150L
            , 400L
    };


    @Test
    public void mainCompetitionTest() {

        Participant[] participants = new Participant[FINAL_ATHLETE.length];
        for (int i = 0; i < FINAL_ATHLETE.length; i++) {
            participants[i] = competition.register(new AthleteImpl(FINAL_ATHLETE[i][0], FINAL_ATHLETE[i][1], FINAL_ATHLETE[i][2]));
            try {
                competition.register(new AthleteImpl(FINAL_ATHLETE[i][0], FINAL_ATHLETE[i][1], FINAL_ATHLETE[i][2]));
                fail("Повторяющая запись");
            } catch ( IllegalArgumentException ex) {
            }

            Athlete athlete = participants[i].getAthlete();
            assertEquals(FINAL_ATHLETE[i][0], athlete.getFirstName() );
            assertEquals(FINAL_ATHLETE[i][1], athlete.getLastName() );
            assertEquals(FINAL_ATHLETE[i][2], athlete.getCountry() );
        }

        for (int i = 0; i < participants.length; i++) {
            competition.updateScore(participants[i].getId(), FINAL_GOALS[i]);
        }
        competition.updateScore(participants[0], 500);
        competition.updateScore(participants[0], -500);


        List<Participant> athletesResults = competition.getResults();
        assertEquals(5, athletesResults.size());
        Participant bestParticipant = athletesResults.get(0);
        Athlete bestAthlete = bestParticipant.getAthlete();
        assertEquals("Игорь", bestAthlete.getFirstName());
        assertEquals("Шелкунов", bestAthlete.getLastName());
        assertEquals("Казахстан", bestAthlete.getCountry());
        assertEquals(500, bestParticipant.getScore());


        List<CountryParticipant> countryResults = competition.getParticipantsCountries();
        CountryParticipant bestCountry = countryResults.get(0);
        assertEquals(3, countryResults.size());
        assertEquals("Казахстан", bestCountry.getName());
        assertEquals(500, bestCountry.getScore());
        assertEquals(3, bestCountry.getParticipants().size());

        bestCountry.toString();
    }
}

